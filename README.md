# Mongodb records manager

Connector to manage the registers of the other connectors in mongodb.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

```bash
git clone https://gitlab.com/gleidernuez/mongodb-records-manager.git
```

### Prerequisites

Docker and docker-compose are required for use

#### Docker

- for windows https://docs.docker.com/docker-for-windows/install/
- for ubuntu https://docs.docker.com/install/linux/docker-ce/ubuntu/
- for centos https://docs.docker.com/install/linux/docker-ce/centos/
- for debian https://docs.docker.com/install/linux/docker-ce/debian/
- for fedora https://docs.docker.com/install/linux/docker-ce/fedora/

#### docker-compose

- for all platform https://docs.docker.com/compose/install/

### Installing

#### Environment

```env
APP_INSTANCE=instance
GIT_MAIN_URL=gitlab.com
GIT_OWNER=ereyes
GIT_USER=user
GIT_PASSWORD=password
```

After having docker and docker-compose ready you only have to execute a command console in the folder where you clone the project and execute the following command.

```bash
docker-compose up
```

## Built With

- [NodeJS](https://nodejs.org/es/) - JavaScript runtime environment
- [npm](https://www.npmjs.com) - Dependency Management
- [autoclone-lib](https://www.npmjs.com/package/autoclone-lib) - Used to automatically clone on reboot
