require("dotenv").config();
const configLocal = require("../config/default.json");

const MongoClient = require("mongodb").MongoClient;
const MONGO_URL = "mongodb://mongodb_connectors:27017";
const DB_NAME = `${configLocal.CONNECTOR_NAME}_${process.env.APP_INSTANCE}`;
const MONGO_COLLECTION = configLocal.MONGO_COLLECTION;
const CONSTANT_FLAG_ID = configLocal.CONSTANT_FLAG_ID;

const connect = async () => {
  try {
    const client = await MongoClient.connect(`${MONGO_URL}/${DB_NAME}`, {
      useNewUrlParser: true,
      loggerLevel: "error",
      useUnifiedTopology: true,
    });
    console.log("Connected successfully to de mongo server");
    return client;
  } catch (error) {
    console.log(error);
  }
};

//////////////////////// SINGLE VALUE MODULE ////////////////////////

const FILTER = { constantFlagId: CONSTANT_FLAG_ID };

const loadRecord = async () => {
  let record = await findOne();
  return record && record.data;
}

const updateRecord = async (data) => {
  return await updateOne({ data });
}

const findOne = async () => {
  const client = await connect();
  try {
    const data = await client
      .db(DB_NAME)
      .collection(MONGO_COLLECTION)
      .findOne(FILTER);
    client.close();
    return data;
  } catch (error) {
    console.error(error);
    client.close();
  }
};

const updateOne = async (ogj) => {
  const client = await connect();
  try {
    const data = await client
      .db(DB_NAME)
      .collection(MONGO_COLLECTION)
      .updateOne(
        FILTER,
        { $set: ogj },
        { upsert: true },
      );
    client.close();
    return data;
  } catch (error) {
    console.log(error);
    client.close();
  }
};

//////////////////////// SINGLE VALUE MODULE ////////////////////////


//////////////////////// IDS LIST MODULE ////////////////////////

const retrieveAll = async () => {
  const client = await connect();
  try {
    const data = await client
      .db(DB_NAME)
      .collection(MONGO_COLLECTION)
      .find()
      .toArray();
    client.close();
    return data;
  } catch (error) {
    console.log(error);
    client.close();
  }
};

const removeAll = async (filter) => {
  filter = filter || {};
  const client = await connect();
  try {
    const data = await client
      .db(DB_NAME)
      .collection(MONGO_COLLECTION)
      .deleteMany(filter);
    client.close();
    return data;
  } catch (error) {
    console.log(error);
    client.close();
  }
};

//////////////////////// IDS LIST MODULE ////////////////////////

const insert = async (obj) => {
  const client = await connect();
  await client
    .db(DB_NAME)
    .collection(MONGO_COLLECTION)
    .insertOne(obj)
    .then((result) => console.log(result))
    .catch((error) => console.log(error));
  client.close();
};

module.exports = {
  insert,

  retrieveAll,
  removeAll,

  loadRecord,
  updateRecord,
};