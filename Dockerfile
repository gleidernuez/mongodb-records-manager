FROM alpine/git AS GIT
FROM node:latest
COPY --from=GIT . .
RUN mkdir -p /usr/src/
RUN mkdir -p /usr/src/git
WORKDIR /usr/src/
COPY package.json package.json
RUN npm install --verbose
COPY . .
CMD ["npm", "start"]