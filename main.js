require("dotenv").config();
const APP_INSTANCE = process.env.APP_INSTANCE;
const configLocal = require("./config/default.json");

const {
  retrieveAll,
  removeAll,
  loadRecord,
  updateRecord,
} = require("./controllers/mongo");

const formatDate = (date) => {
  try {
    const digit = (digit) => `00${digit}`.substr(-2);
    date = date instanceof Date ? date : new Date(date);
    let year = date.getUTCFullYear();
    let month = digit(date.getUTCMonth() + 1);
    let day = digit(date.getUTCDate());
    return `${year}-${month}-${day}`;
  } catch (error) {
    console.error("Error on formatDate:", error);
    throw error;
  }
}

const checkIfItIsNotOnDate = () => {
  const controlDate = new Date(
    formatDate(configLocal.SECURITY_CHECK_DATE)
  );
  const currentDate = new Date(
    formatDate(new Date())
  );
  return controlDate.getTime() !== currentDate.getTime();
}

const spaceInConsole = () => {
  console.log("|");
}

const frameText = (text) => {
  return ` --->> ${text} <<---`;
}

const finalMessage = (text) => {
  text = frameText(text);
  let interval = 60;
  console.log(text);
  setInterval(() => {
    console.log(text);
  }, interval * 1000);
}

const singleValueModule = async () => {
  console.log(frameText("SINGLE VALUE MODULE"));
  const CONFIG = configLocal.SINGLE_VALUE_MODULE;
  if (!CONFIG.ACTIVE) {
    console.log(frameText("It is not active !!!"));
    return;
  }

  console.log("Restart or update the registry in mongodb ...");

  let record = await loadRecord();
  console.log("Record:", record);

  if (CONFIG.DELETE_RECORD) {
    console.log("Delete all register...");
    await removeAll();
  } else if (CONFIG.UPDATE_RECORD) {
    console.log("Update record:", CONFIG.UPDATE_RECORD);
    await updateRecord(CONFIG.UPDATE_RECORD);
  }

  record = await loadRecord();
  console.log("Record:", record);
}

const idsListModule = async () => {
  console.log(frameText("IDS LIST MODULE"));
  const CONFIG = configLocal.IDS_LIST_MODULE;
  if (!CONFIG.ACTIVE) {
    console.log(frameText("It is not active !!!"));
    return;
  }
  console.log("Restart the registry in mongodb ...");

  let logs = await retrieveAll();
  console.log("Total logs:", logs && logs.length);

  if (CONFIG.DELETE_ALL_RECORDS) {
    console.log("Delete all register...");
    await removeAll();
  } else {
    const list = CONFIG.DELETE_RECORD_LIST || [];
    let i = 0;
    while (i < list.length) {
      let _id = list[i];
      console.log("Delete register:", _id);
      await removeAll({ _id });
      i++;
    }
  }

  logs = await retrieveAll();
  console.log("Total logs:", logs && logs.length);
}

(async () => {
  console.log("Main...");

  if (checkIfItIsNotOnDate()) {
    finalMessage("WARNING, the control date is not equal to the current date.");
    return;
  }

  spaceInConsole();
  console.log("Environment Variables:");

  const DB_NAME = `${configLocal.CONNECTOR_NAME}_${APP_INSTANCE}`;
  const MONGO_COLLECTION = configLocal.MONGO_COLLECTION;
  const CONSTANT_FLAG_ID = configLocal.CONSTANT_FLAG_ID;

  console.log("APP_INSTANCE     :", APP_INSTANCE);
  console.log("DB_NAME          :", DB_NAME);
  console.log("MONGO_COLLECTION :", MONGO_COLLECTION);
  console.log("CONSTANT_FLAG_ID :", CONSTANT_FLAG_ID);

  spaceInConsole();
  await idsListModule();

  spaceInConsole();
  await singleValueModule();

  spaceInConsole();
  finalMessage("MongDB reboot finished.");

})();