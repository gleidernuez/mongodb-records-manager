const { init } = require("autoclone-lib");

(async () => {
  await init(require("./package.json").name);
  require("./main");
})()